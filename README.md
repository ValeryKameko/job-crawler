# Job Crawler

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/job_crawler`. To experiment with that code, run `bin/console` for an interactive prompt.

Application, that collects jobs from collection of sources and saves result in XML files

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'job_crawler'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install job_crawler

## Usage

Just run:

    $ ./bin/job-crawler

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/ValeryKameko/job-crawler


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
