require 'lib/job_crawler/job_source_parser/boehringertaleo_job_parser.rb'

RSpec.describe BoehringerTaleoJobParser do
#   describe '#parse_all_job_links' do
#     it 'parse all job links' do
#       parser = BoehringerTaleoJobParser.new
#       links = parser.parse_all_job_links.lazy.take(20).to_a
#       expect(links.length).to be > 0
#       expect(links).to all start_with('http')
#     end
#   end

  describe '#parse_all_jobs' do
    it 'parse all jobs' do
      parser = BoehringerTaleoJobParser.new
      jobs = parser.parse_all_jobs.take(1).to_a
      expect(jobs.length).to be > 0
      expect(jobs).to all satisfy { |job|
        expect(job.url.to_s).to start_with('http')
        expect(job.id).to match(/^(\w|\d)+$/)
        expect(job.title).not_to be_empty
        expect(job.description).not_to be_empty
      }
    end
  end
end
