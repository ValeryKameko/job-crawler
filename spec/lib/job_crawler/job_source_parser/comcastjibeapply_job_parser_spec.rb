require 'lib/job_crawler/job_source_parser/comcastjibeapply_job_parser.rb'

RSpec.describe ComcastJibeapplyJobParser do
  describe '#parse_all_jobs' do
    it 'parse all jobs' do
      parser = ComcastJibeapplyJobParser.new
      jobs = parser.parse_all_jobs.take(1).to_a
      expect(jobs.length).to be > 0
      expect(jobs).to all satisfy { |job|
        expect(job.url.to_s).to start_with('http')
        expect(job.id).to match(/^(\w|\d)+$/)
        expect(job.title).not_to be_empty
        expect(job.description).not_to be_empty
      }
    end
  end
end
