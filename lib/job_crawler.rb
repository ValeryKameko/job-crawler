require 'fileutils'
require 'job_crawler/version'
require_relative './job_crawler/job_source_parser/shaderimaging_job_parser.rb'
require_relative './job_crawler/job_source_parser/comcastjibeapply_job_parser.rb'
require_relative './job_crawler/job_source_parser/boehringertaleo_job_parser.rb'
require_relative './job_crawler/jobs_serializer.rb'

module JobCrawler
  PARSERS = {
    'shared imaging': SharedImagingJobParser.new,
    'comcast jibeapply': ComcastJibeapplyJobParser.new,
    'boehringer taleo': BoehringerTaleoJobParser.new
  }.freeze

  def self.crawl_all_jobs(save_dir: 'data')
    serializer = JobsSerializer.new
    PARSERS.each do |parser_name, parser|
      jobs = parser.parse_all_jobs.take(20)
      serialized_jobs = serializer.serialize_jobs(jobs)
      save_jobs_data(
        save_dir: save_dir,
        file_name: parser_name,
        jobs_data: serialized_jobs)
    end
  end

  private

  def self.save_jobs_data(save_dir:, file_name:, jobs_data:)
    FileUtils.mkdir_p(save_dir)
    save_path = File.join(save_dir, "#{file_name}.xml")
    File.write(save_path, jobs_data, mode: 'w')
  end
end
