require 'mechanize'

class JobSourceParser
  def create_crawler_agent
    agent = Mechanize.new
    agent.user_agent_alias = 'Linux Firefox'
    agent.request_headers =  { 'Accept-Encoding' => '' }
    agent
  end
end
