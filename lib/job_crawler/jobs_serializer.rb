require 'ox'

class JobsSerializer
  def serialize_jobs(jobs)
    doc = Ox::Document.new(version: '1.0')
    jobs_element = Ox::Element.new('jobs')
    doc << jobs_element

    jobs.each do |job|
      jobs_element << serialize_job(job)
    end
    Ox.dump(doc)
  end

  def serialize_job(job)
    job_element = Ox::Element.new('job')
    map_job(job).each do |key, value|
      job_element << (Ox::Element.new(key.to_s) << value.to_s)
    end
    job_element
  end

  def map_job(job)
    {
      url: job.url,
      id: job.id,
      title: job.title,
      location: job.location,
      description: job.description
    }
  end
end
