Job = Struct.new(
  :url,
  :id,
  :title,
  :location,
  :description,
  keyword_init: true
) do
  def initialize(
    url:,
    id:,
    title:,
    location:,
    description:)
    super
  end
end
