require 'json'
require_relative '../job_source_parser.rb'
require_relative '../job.rb'

class ComcastJibeapplyJobParser < JobSourceParser
  JOBS_PAGE_URL = 'https://comcast.jibeapply.com/api/jobs'

  def initialize
    @agent = create_crawler_agent
  end

  def parse_all_jobs
    Enumerator.new do |y|
      1.step.each do |page_number|
        page = @agent.get("#{JOBS_PAGE_URL}?page=#{page_number}")
        json = JSON.parse(page.body)
        break if json['jobs'].count.zero?

        json['jobs'].each do |job_json|
          y << parse_job(job_json['data'])
        end
      end
    end
  end

  def parse_job(job_json)
    Job.new(
      url: job_json['meta_data']['canonical_url'],
      id: job_json['req_id'],
      title: job_json['title'],
      location: job_json['full_location'],
      description: job_json['description'])
  end
end
