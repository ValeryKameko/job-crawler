require 'cgi'
require_relative '../job_source_parser.rb'
require_relative '../job.rb'

class BoehringerTaleoJobParser < JobSourceParser
  JOBS_PAGE_URL = 'https://boehringer.taleo.net/careersection/2/jobsearch.ftl'
  JOBS_AJAX_URL = 'https://boehringer.taleo.net/careersection/2/jobsearch.ajax'
  REQUIRED_AJAX_VALUES = {
    'iframemode' => '1',
    'ftlpageid' => 'reqListBasicPage',
    'ftlinterfaceid' => 'requisitionListInterface',
    'ftlcompid' => 'rlPager',
    'jsfCmdId' => 'rlPager',
    'ftlcompclass' => 'PagerComponent',
    'ftlcallback' => 'ftlPager_processResponse',
    'ftlajaxid' => 'ftlx1',
    'tz' => 'GMT+03:00'
  }.freeze
  JOB_DATA_ID_POSITION = 0
  JOB_DATA_TITLE_POSITION = 4
  JOB_DATA_URL_POSITION = 9
  JOB_DATA_DESCRIPTION_POSITION = 14
  JOB_DATA_LOCATION_POSITION = 17

  def initialize
    @agent = create_crawler_agent
    @page_agent = create_crawler_agent
  end

  def parse_all_jobs
    jobs_page = @agent.get(JOBS_PAGE_URL)
    Enumerator.new do |y|
      (1..).each do |page|
        form_data = collect_form_data(jobs_page, page)
        page_data = @agent.post(JOBS_AJAX_URL, form_data)
        parse_job_links(page_data).each do |link|
          job_page = @page_agent.get(link)
          y << parse_job(job_page)
        end
      end
    end
  end

  def collect_form_data(job_page, page_id)
    data = (job_page / '#ftlform input').map do |input|
      [input['name'].to_s, input['value']]
    end.to_h
    data.delete_if { |key, value| key.empty? || value.nil? }
    data.update(REQUIRED_AJAX_VALUES)
    data['rlPager.currentPage'] = page_id.to_s
    data
  end

  def parse_job_links(page_data)
    response = (page_data % '[id="response"]').content
    response.scan(/job=(\d+)/).map do |match|
      "https://boehringer.taleo.net/careersection/jobdetail.ftl?job=#{match[0]}"
    end
  end

  def parse_job(job_page)
    script = (job_page % 'script:not([src])').content
    setup_function = script[/setup: function\(\) \{([^\}]+)\}/, 1]
    description = setup_function[/'descRequisition', \[([^\[]+)\]/, 1]
    description_list = description.scan(/'([^']*)'/).map { |match| match[0] }

    Job.new(
      url: description_list[JOB_DATA_URL_POSITION],
      id: description_list[JOB_DATA_ID_POSITION],
      title: description_list[JOB_DATA_TITLE_POSITION],
      location: description_list[JOB_DATA_LOCATION_POSITION],
      description: decode_description(description_list[JOB_DATA_DESCRIPTION_POSITION]))
  end

  def decode_description(description)
    return CGI.unescape(description[3..])
  end
end