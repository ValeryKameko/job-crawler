require_relative '../job_source_parser.rb'
require_relative '../job.rb'

class SharedImagingJobParser < JobSourceParser
  JOBS_PAGE_URL = 'https://sharedimaging.isolvedhire.com/jobs/'
  JOB_URL_SELECTOR = '#job_listings > .job-listings > a.list-group-item'
  JOB_TITLE_SELECTOR = 'h1 > a'
  JOB_LOCATION_SELECTOR = '.job-items > li > span:nth-child(2)'
  JOB_DESCRIPTION_SELECTOR = '#mainArea > div.panel-body > div > div.col-sm-7 > div:nth-child(3)'

  def initialize
    @agent = create_crawler_agent
  end

  def parse_all_jobs
    parse_all_job_links.lazy.map do |job_link|
      job_page = @agent.get(job_link)
      parse_job(job_page: job_page)
    end
  end

  def parse_all_job_links
    jobs_page = @agent.get(JOBS_PAGE_URL)

    Enumerator.new do |y|
      (jobs_page / JOB_URL_SELECTOR).each do |job_element|
        y << job_element['href'].to_s
      end
    end
  end

  def parse_job(job_page:)
    Job.new(
      url: job_page.uri.to_s,
      id: job_page.uri.to_s[/\d+/],
      title: (job_page % JOB_TITLE_SELECTOR).text,
      location: (job_page % JOB_LOCATION_SELECTOR).text,
      description: (job_page % JOB_DESCRIPTION_SELECTOR).inner_html)
  end
end
