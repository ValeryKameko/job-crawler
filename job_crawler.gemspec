require_relative "lib/job_crawler/version"

Gem::Specification.new do |spec|
  spec.name          = "job_crawler"
  spec.version       = JobCrawler::VERSION
  spec.authors       = ["Valery Kameko"]
  spec.email         = ["vkamieko@gmail.com"]

  spec.summary       = %q(Gem, that provides functionality to collect jobs from collection of sources and saving results in XML files)
  spec.homepage      = "https://gitlab.com/ValeryKameko/job-crawler"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.6.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/ValeryKameko/job-crawler"

  spec.files         = Dir.chdir(File.expand_path("..", __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rubocop"
  spec.add_dependency "mechanize"
  spec.add_dependency "ox"
end
